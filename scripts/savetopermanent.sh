#!/bin/sh
echo "\n"
echo "Saving JIRA Data"
DB_PATH=../data/jira/database
H2_JAR=../lib/h2-1.4.185.jar
# export data from h2 db
java -cp $H2_JAR org.h2.tools.Script -script $DB_PATH/jiradb.script -user sa -url "jdbc:h2:file:$DB_PATH/h2db"
# move to permanent
php migrate_dates.php saveDatesPermanent $DB_PATH/jiradb.script $DB_PATH/jiradb-permanent.script
# remove jiradb.script
rm -f $DB_PATH/jiradb.script

echo "\n"
echo "Saving Confluence Data"
php migrate_dates.php saveDatesPermanent ../data/confluence/database/confluencedb.script ../data/confluence/database/confluencedb-permanent.script
